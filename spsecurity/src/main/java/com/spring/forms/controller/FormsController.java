package com.spring.forms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.forms.bean.User;

@Controller
@RequestMapping("/forms")
public class FormsController {

	

	@RequestMapping("/register")
	public String register(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}
	
	/*@RequestMapping("/login")
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}*/
	
	@RequestMapping("/update")
	public String update(@RequestParam(value="id") Integer id,Model model) {
		System.out.println("Updating Userid: " + id);
		return "update";
	}

	
}
