package com.spring.forms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.forms.bean.User;

@Controller
public class NavbarController {

	@RequestMapping("/")
	public String index() {
		System.out.println("Requesting index...");
		return "index";
	}

	@RequestMapping("/services")
	public String getServices() {
		return "services";
	}

	@RequestMapping("/appointments")
	public String getAppointments() {
		return "appointments";
	}

	@RequestMapping("/schedules")
	public String getSchedules() {
		return "schedules";
	}

	@RequestMapping("/login")
	public String login(Model model) {
		/* model.addAttribute("user", new User()); */
		return "login";
	}
	
	@RequestMapping("/logout")
	public String logout(Model model) {
		/* model.addAttribute("user", new User()); */
		return "redirect:/";
	}

}
