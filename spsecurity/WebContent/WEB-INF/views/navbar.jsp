<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="${pageContext.request.contextPath}">PortalApp</a>

		</div>
		<ul class="nav navbar-nav">
			<li><a href="${pageContext.request.contextPath}/services">Services</a></li>
			<li><a href="${pageContext.request.contextPath}/appointments">Appointments</a></li>
			<li><a href="${pageContext.request.contextPath}/schedules">Schedules</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<%-- <li>
				<p class="navbar-text">

					Welcome,
					<sec:authentication property="name" />
					| <a href='<c:url value="/logout"/>'><span
						class="glyphicon glyphicon-log-out"></span>Logout</a>
				</p>
			</li> --%>
			<!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span>
						Sign Up</a></li> -->

			<sec:authorize access="isAuthenticated()" var="authenticated" />

			<c:choose>
				<c:when test="${authenticated}">
					<li>
						<p class="navbar-text">
							Welcome, <span class="glyphicon glyphicon-user"></span>
							<sec:authentication property="name" />
							| <a id="logout" href='#'><span
								class="glyphicon glyphicon-log-out"></span>Logout</a>
						</p>
						<form id="logout-form" action='<c:url value="/logout"/>'
							method="post">
							<sec:csrfInput />
						</form>
					</li>
				</c:when>

				<c:otherwise>
					<li><a href="${pageContext.request.contextPath }/login"> <span
							class="glyphicon glyphicon-log-in"></span> Login
					</a></li>
				</c:otherwise>
			</c:choose>

		</ul>
	</div>
	<div class="line"></div>
	</nav>

</body>
<script>
	$(document).ready(function(e) {

		$("#logout").click(function(e) {
			e.preventDefault();
			$("#logout-form").submit();
		});

	});
</script>