<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Register User</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<%-- <nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a href="${pageContext.request.contextPath}/" class="navbar-brand">PortalApp</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="#">Page</a></li>
			<li><a href="#">Page</a></li>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<li class="active"><a href="./register/">Register</a></li>
			<li><a href="#">Login</a></li>
		</ul>
	</div>
	</nav> --%>
	
	<jsp:include page="navbar.jsp" />

	<div class="contianer">

		<div class="panel panel-default">
			<div class="panel-heading">Registration</div>
			<div class="panel-body">
				`
				<div class="col-md-6">
					<form:form action="../user/save" modelAttribute="user" class="form-vertical">

						<div class="form-group">
							<label class="control-label col-md-2" for="email">Name:</label>
							<div class="co-md-10">
								<form:input type="text" path="name" value="Jack" class="form-control" />
							</div>
						</div>
						<label class="control-label col-md-2" for="email">Contact:</label>
						<div class="form-group">
							<div class="co-md-10">
								<form:input type="text" path="contact" value="112233" class="form-control" />
							</div>
						</div>
						<label class="control-label col-md-2" for="email">Email:</label>
						<div class="form-group">
							<div class="co-md-10">
								<form:input type="text" path="email" value="jack@mail.com" class="form-control" />
							</div>
						</div>

						<input type="submit" value="Register" class="btn">

					</form:form>
				</div>

			</div>
		</div>

	</div>

</body>
</html>