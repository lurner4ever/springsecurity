<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<!-- Site Properties -->
<title>Login</title>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="panel panel-default" style="margin-top: 50px;">
			<div class="panel-heading">Login</div>
			<div class="panel-body">
				<c:url value="/login" var="loginVar" />
				<form class="form-horizontal" role="form" method="POST"
					action="${loginVar}">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">

							<c:if test="${param.error != null}">
								<p style="color: red">Invalid Credentials.Try Again.</p>
							</c:if>
							
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group has-danger">
								<div class="col-md-6">
									<input type="text" name="username" class="form-control"
										id="email" placeholder="Username" required autofocus>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-control-feedback"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-6">
									<input type="password" name="password" class="form-control"
										id="password" placeholder="Password" required>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-control-feedback">
								<span class="text-danger align-middle"> <!-- Put password error message here -->
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6" style="padding-top: .35rem">
							<div class="form-check mb-2 mr-sm-2 mb-sm-0">
								<label class="form-check-label"> <input
									class="form-check-input" name="remember" type="checkbox">
									<span style="padding-bottom: .15rem">Remember me</span>
								</label>
							</div>
						</div>
					</div>

					<div class="row" style="padding-top: 1rem">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<sec:csrfInput />

							<button type="submit" class="btn btn-success">
								<i class="fa fa-sign-in"></i> Login
							</button>

							<a class="btn btn-link" href="/password/reset">Forgot Your
								Password?</a>| <a class="btn btn-link" href="#">Register</a>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>

</body>
</html>